open List


let max (a: int) (b: int): int  =
if a > b then a else b
  
  (** ///////////////////////////////////////////////////////////
    Some extra functions for List
    /////////////////////////////////////////////////////////// **)
  
let rec max_list (l:int list):int =
  match l with
      [] -> failwith "no element in the list"
    | [e] -> e
    | e::lp -> let mlp = max_list lp in if e > mlp then e else mlp

let rec min_list (l:int list):int =
  match l with
      [] -> failwith "no element in the list"
    | [e] -> e
    | e::lp -> let mlp = min_list lp in if e < mlp then e else mlp

let rec equal_list (l1:'a list) (l2: 'a list):bool =
    match l1, l2 with
	[], [] -> true
      | e1::r1, e2::r2 -> e1 = e2 && equal_list r1 r2
      | _ -> false

let make_list (size:int) (fill_function:int -> 'a):'a list =
  let rec list_of_indexes n = if n=1 then [1] else (list_of_indexes (n-1))@[n] in
		map fill_function (list_of_indexes size)
		
let map3 (f:'a -> 'b -> 'c -> 'd) (a:'a list) (b:'b list) (c:'c list) =
	map2 (fun x (y1,y2) -> f x y1 y2) a (map2 (fun x y -> (x,y)) b c)

let iter3 (f:'a -> 'b -> 'c -> unit) (a:'a list) (b:'b list) (c:'c list) =
	iter2 (fun x (y1,y2) -> f x y1 y2) a (map2 (fun x y -> (x,y)) b c)


let rec prefix (n:int) (l:'a list) =
	let rec prefix_rec (n:int) (l:'a list) (r:'a list) =
		if (n=0) then r
		else
			match l with
				[] -> failwith "error argument l"
				| head::remainder -> 
			prefix_rec (n-1) remainder (r @ [head])
	in 
		prefix_rec n l []

let rec suppress_duplicates (l:'a list) : 'a list =
  match l with
    | [] -> []
    | [e] -> [e]
    | e::rem -> let remnodupes = suppress_duplicates rem in
		(if (mem e rem) then [] else [e])@remnodupes  
		  
(** //////////////////////////////////////////////////////////////////
    Shared string representation of some data structure
    ////////////////////////////////////////////////////////////// **)
let stringrep_intint ((x,y): int*int) : string =
  "("^(string_of_int x)^","^(string_of_int y)^")"

open Printf

let stringrep_list (srep_element: 'a -> string) (l: 'a list) : string =
  let body = List.fold_left (fun acc elt -> acc^(if acc = "" then "" else ",")^(srep_element elt)) "" l in
  "["^body^"]"
    
let stringrep_intint_list (l:(int*int) list): string =
 stringrep_list stringrep_intint l
    
let stringrep_int_list (l:int list): string =
  stringrep_list string_of_int l


let stringrep_array (a: 'a array) (srep_element: 'a -> string) (return:bool) : string =
  let size = Array.length a in
  let possible_return = (if return then "\n" else "") in
  let rec sr (index:int) : string =
    if (index > size - 1) then
      ""
    else
      (
	let string_of_element = (if index <> 0 then possible_return else "")^(string_of_int index)^":"^" "^(srep_element a.(index)) in
	(if (index <> 0 && not return) then ", " else "")^string_of_element^(sr (index + 1))
      )
  in
  "["^possible_return^(sr 0)^possible_return^"]"

let stringrep_arrayarray (a: 'a array array) (srep_element: 'a -> string) : string =
  stringrep_array a (fun x -> stringrep_array x srep_element false) true

(**
Writes to a file (append mode)
Does not make any assumption on the file.
**)
let write_to_file (file_name:string) (content:string) =
  let oc = open_out_gen [Open_creat; Open_text; Open_append] 0o666 file_name in    (* create or truncate file, return channel *)
  fprintf oc "%s\n" content;   (* write something *)
  close_out oc; (* flush and close the channel *)
    

			 

			 
