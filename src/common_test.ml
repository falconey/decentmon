open Utils
open List
open Alphabetevent
open Trace
open Architecture
open Ltl
open Dltl
open Ltl_generator
open Network
open Printers
open Common_network
open Dyn_network
open C_monitoring
open Od_monitoring
open Md_monitoring



(** ///////////////////////////////////////////////////////////
    Utils
    ///////////////////////////////////////////////////////////**)

let test_verbose : bool ref = ref true
let error_verbose : bool ref = ref true

let separator_line = "///********************************************///"
  
let print_separator_line () : unit =
  print_endline (separator_line)

let test_message (msg:string) : unit =
  if (!test_verbose) then
    print_endline (msg)
      
let error_message (msg:string) : unit =
  if (!error_verbose) then (
    print_separator_line ();
    print_endline ("ERROR: "^msg);
    print_separator_line ();
  )

(**
   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////
   Tests related to static
   networks
   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////**)
    
(** ///////////////////////////////////////////////////////////
    Test the creation of a static network
    ///////////////////////////////////////////////////////////**)

let test_network (archi: (string list) Architecture.t) (phi:dltl) : network =
  print_separator_line ();
  test_message ("Considered architecture:\n"^stringrep_architecture archi);
  test_message ("Considered formula: "^string_rep phi);
  let chc_phi = chc phi archi 0 in
  test_message ("Chosen initial component: "^string_of_int chc_phi);
  let empty_network = make_empty_network (Architecture.cardinal archi) in
  let newnet = network archi chc_phi empty_network phi in
  test_message ("Resulting network:\n"^stringrep_network newnet);
  newnet

(** //////////////////////////////////////////////////////////////
    Test the computation of compacting the network
    ///////////////////////////////////////////////////////////**)
    
let test_compact (n:network) : network =
  print_separator_line ();
  test_message ("Input network (before compacting):\n"^stringrep_network n);
  let compactnet = compact n in
  test_message ("Resulting network (after compacting):\n"^stringrep_network compactnet);
  compactnet

(** //////////////////////////////////////////////////////////////
    Test the reindexation of the cell of the network after a
    compacting.
    ///////////////////////////////////////////////////////////**)
    
let test_reindex (n:network) : network =
  print_separator_line ();
  test_message ("Initial network (before reindexing):\n"^stringrep_network n);
  let reindexnet = reindex n in
  test_message ("Resulting network (after reindexing):\n"^stringrep_network reindexnet);
  reindexnet

(** //////////////////////////////////////////////////////////////
    Test the computation of automatically respawning cells in a static
    network
    ///////////////////////////////////////////////////////////**)
let test_respawn (n:network) (archi:architecture) (phi:dltl) : coordinates list =
  print_separator_line ();
  test_message ("Computing automatically respawning cells for the following network:");
  test_message (stringrep_network n);
  let respawning = compute_respawn archi n phi in
  let srep_respawning = stringrep_intint_list respawning in
  test_message ("Automatically respawning cells:");
  test_message (srep_respawning);
  respawning

(** //////////////////////////////////////////////////////////////
    Test the computation of referents of a network
    ///////////////////////////////////////////////////////////**)
let test_referents (n:network) : referents_network =
  print_separator_line ();
  test_message ("Computing referents for the following network:");
  test_message (stringrep_network n);
  let refs = referents_of_network n in
  test_message ("Referents are:");  
  test_message (stringrep_referents_network refs);
  refs

(** //////////////////////////////////////////////////////////////
    Test the computation of referents of a network
    ///////////////////////////////////////////////////////////**)
let test_referrers (n:network) : referrers_network =
  print_separator_line ();
  test_message ("Computing referrers for the following network:");
  test_message (stringrep_network n);
  let refs = referrers_of_network n in
  test_message ("Referrers are:");  
  test_message (stringrep_referrers_network refs);
  refs
    
(** ///////////////////////////////////////////////////////////
    Launching all tests all at once
    ///////////////////////////////////////////////////////////**)

let test_static_network (phi:dltl) (dalpha:d_alphabet) : network =
  let archi = dalphabet_2_architecture dalpha in
  let net = test_network archi phi in
  let net_compact = test_compact net in
  let net_reindex = test_reindex net_compact in
  let _ = test_respawn net_reindex archi phi in
  let _ = test_referents net_reindex in
  let _ = test_referrers net_reindex in
  net_reindex
    
    
(** //////////////////////////////////////////////////////////////
    Test the conversion from a static network to a dynamic network
    ///////////////////////////////////////////////////////////**)

let test_conversion (n:network) : dyn_net =
  print_separator_line ();
  test_message ("Converting the following static network");
  test_message (stringrep_network n);
  test_message ("Into the following dynamic network");
  let dynnet = make_dyn_net n in
  test_message (stringrep_dyn_net dynnet);
  dynnet


(** /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    Tests related to orchestration based-monitoring (compared to centralised monitoring)
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////**)

(** //////////////////////////////////////////////////////////////
    Test the decentralised monitoring of a trace against a
    property.  Trace and property are passed as parameters.
    ///////////////////////////////////////////////////////////**)

let test_dmonitoring_1trace_1formula (dalpha:d_alphabet) (phi:dltl) (t:trace) : unit =
  let archi = dalphabet_2_architecture dalpha in
  print_separator_line ();
  test_message ("Considered architecture:\n"^stringrep_architecture archi);
  test_message ("Considered formula: "^string_rep phi);
  test_message ("Considered trace: "^stringrep_trace t);
  test_message ("Launching monitoring.");
  let _ = od_monitor archi phi t in ()

(** //////////////////////////////////////////////////////////////
    Test the decentralised monitoring of a trace against a property.
    Trace and property are randomly generated.
    ///////////////////////////////////////////////////////////**)
let test_dmonitoring_1randomTrace_1randomFormula (dalpha:d_alphabet) (size_form:int) (size_trace:int) =
  let alpha = globalAlphabet dalpha in
  let _ = test_dmonitoring_1trace_1formula dalpha (ltl_2_dltl (gen_1_form_nontrivial size_form alpha)) (gen_1_trace size_trace alpha)
  in ()

(** /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    Functional test of the monitoring algorithm
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////**)

let test_correctness_monitoring_algo (dalpha:d_alphabet) (phi:dltl) (t:trace) : unit =
  let archi = dalphabet_2_architecture dalpha and
      alpha = globalAlphabet dalpha in
  print_separator_line ();
  test_message ("Considered architecture:\n"^stringrep_architecture archi);
  test_message ("Considered formula: "^string_rep phi);
  test_message ("Considered trace: "^stringrep_trace t);
  test_message ("Launching monitoring.");
  let vdmon = od_monitor archi phi t and (vcmon,_,_) = cmonitor (dltl_2_ltl phi) t in
  if (vdmon <> vcmon) then
    error_message ("Different verdicts when monitoring when formula "^string_rep phi^" on trace "^stringrep_trace t)

let test_correctness_monitoring_algo_1randomTrace_1Formula (dalpha:d_alphabet) (phi:dltl) (size_trace:int) =
  let phi = simp phi in
  let alpha = globalAlphabet dalpha in
  let _ = test_correctness_monitoring_algo dalpha phi (gen_1_trace size_trace alpha)
  in ()

let test_correctness_monitoring_algo_XrandomTraces_1Formula (dalpha:d_alphabet) (phi:dltl) (nb_traces:int) (size_trace:int) =
  let phi = simp phi in
  let alpha = globalAlphabet dalpha in
  for cpt = 1 to nb_traces do
  test_correctness_monitoring_algo dalpha phi (gen_1_trace size_trace alpha)
  done

let test_correctness_monitoring_algo_1randomTrace_1randomFormula (dalpha:d_alphabet) (size_form:int) (size_trace:int) =
  let alpha = globalAlphabet dalpha in
  let _ = test_correctness_monitoring_algo dalpha (ltl_2_dltl (gen_1_form_nontrivial size_form alpha)) (gen_1_trace size_trace alpha)
  in ()

let test_correctness_monitoring_algo_X_randomTraces_randomFormulae (nbTests:int) (dalpha:d_alphabet) (size_form:int) (size_trace:int) =
  for cpt = 1 to nbTests do
    test_correctness_monitoring_algo_1randomTrace_1randomFormula dalpha size_form size_trace
  done


    (** /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    Comparion tests related to orchestration-based vs migration-based
    monitoring
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////**)

let rec how_many_props_differ (e1:event) (e2:event) (alpha:alphabet):int =
  match alpha with
      [] -> 0
    | p::tail -> (if ((List.mem p e1 && List.mem p e2) || not (List.mem p e1 || List.mem p e2)) then 0 else 1) + how_many_props_differ e1 e2 tail

let number_ofupdown_fronts (t:d_trace) (dalpha:d_alphabet) = 
  let rec number_of_updwon_fronts_rec (t:d_trace) (dalpha:d_alphabet)  (acc:int) =
    match t with
	[] -> acc
      | elt::[] -> acc
      | e1::e2::remainder -> 
	let fronts_current_event = List.fold_left (fun x y -> x + (if y>0 then 1 else 0)) 0 (map3 how_many_props_differ e1 e2 dalpha) in
	number_of_updwon_fronts_rec (e2::remainder) dalpha (fronts_current_event+acc)
  in number_of_updwon_fronts_rec t dalpha (List.length dalpha)

  
type message_passing = SEND_EVERYTHING | SEND_CHANGES

let mode  = ref SEND_EVERYTHING 

let number_of_messages_centralized_case (traceNeededCent:trace) (traceNeededDecent:d_trace) (dalpha:d_alphabet) =

  match !mode with
    | SEND_EVERYTHING -> (List.length traceNeededCent) * (List.length dalpha)
    | SEND_CHANGES -> 
      let the_prefix = (prefix ((length traceNeededCent)) traceNeededDecent) in 
      let tmp = number_ofupdown_fronts the_prefix dalpha in
      (*
	print_endline (if the_prefix = [] then "empty" else (string_rep_dtrace the_prefix));
	print_int tmp; 
      *)
      tmp (* a bit tricky here. We want the prefix of the d_trace of length of the (c_)trace; because the function number_of_updown_fronts uses the d_trace directly and we want to know what would be the number of messages if there was a central observation point that collects the observation coming from the d_tracec *)
		

let size_of_messages_cent (traceNeededCent:trace) (dalpha:d_alphabet) =
  float_of_int (length (globalAlphabet dalpha)) /. float_of_int (length dalpha)

(*
Compares centralized and decentralized monitoring in terms of trace needed to get a verdict and number of messages exchanged
*)
let compare_results (formula:ltl) (alpha:d_alphabet) (trace:d_trace) =
  
  let size = List.length alpha in
  (
    let global_trace = globalTrace trace and
	array_of_form = Array.make size formula and
	init_oblig = Array.make size (True:ltl) in
    let (verdict_centralized,traceTmp,nb_progressions_cent) = cmonitor formula global_trace and
	(verdict_Mdecent,traceTmpMDecent, num_mess_Mdecent, size_mess_Mdecent, nb_progressions_decent) = md_monitor array_of_form alpha trace 0 init_oblig trace [] 0 0. and
	(verdict_Odecent, traceTmpODecent, num_mess_Odecent, size_mess_Odecent, nb_progressions_odecent) = od_monitor_stats (dalphabet_2_architecture alpha) (ltl_2_dltl formula) (globalTrace trace) in
    (
      let num_mess_cent = number_of_messages_centralized_case traceTmp traceTmpMDecent alpha in
      let size_mess_cent = size_of_messages_cent traceTmp alpha in
      (*the total number of exchanged messages is the size of the d_alphabet * the length of the trace*)
      (**
	 if (List.length traceTmp > List.length traceTmpMDecent) then
	 (
	 print_endline ("Error for formula :"^Ltl.string_rep formula);
	 print_endline ("initial dtrace : "^stringrep_dtrace trace);
	 print_endline ("trace : "^stringrep_trace traceTmp);
	 print_endline ("dtrace : "^stringrep_dtrace traceTmpMDecent);
	 );
      **)
      (formula, trace, verdict_centralized, verdict_Mdecent, verdict_Odecent, traceTmp, traceTmpMDecent, traceTmpODecent, num_mess_cent, num_mess_Mdecent, num_mess_Odecent, size_mess_cent, size_mess_Mdecent, size_mess_Odecent, nb_progressions_cent, nb_progressions_decent, nb_progressions_odecent)
    )
  )

let rec generate_compared_results_efficient (alpha:d_alphabet) (size_form:int) (size_trace:int) (bias:bool) =
  let trace = gen_1_dtrace size_trace alpha
  and formula : ltl =
    if (bias) then
      Ltl.simp (Ltl_generator.gen_1_form_nontrivial_biased size_form alpha (Random.int (length alpha)))
    else
      Ltl.simp (Ltl_generator.gen_1_form_nontrivial size_form (globalAlphabet alpha))
  in
  let (formula, trace,
       verdict_cent, verdict_Mdecent, verdict_Odecent,
       traceNeededCent,traceNeededMDeCent,traceNeededODecent,
       ncent, nMDecent, nODecent,
       smesscent, smessdecent, smessodecent,
  nbProgCent, nbProgDecent, nbProgODecent) = 
    compare_results formula alpha trace in
  let meaningful = verdict_cent = verdict_Mdecent && verdict_cent = verdict_Odecent && List.length traceNeededCent < size_trace && List.length traceNeededMDeCent < size_trace && List.length traceNeededODecent < size_trace && List.length traceNeededCent > 3* (size_form -1) in
  (meaningful, List.length traceNeededCent + 1 , List.length traceNeededMDeCent + 1, List.length traceNeededODecent + 1,ncent,nMDecent,nODecent,smesscent,smessdecent,smessodecent, nbProgCent, nbProgDecent, nbProgODecent)


let generate_compared_results_efficient_patterns (alpha:d_alphabet) (kind:string) (size_trace:int)  =
  let trace = gen_1_dtrace size_trace alpha
  and formula :ltl =
    if (kind="abs") then Ltl.simp (gen_1_form_abscence (globalAlphabet alpha))
    else if (kind="exist") then Ltl.simp (gen_1_form_existence (globalAlphabet alpha))
    else if (kind="unive") then Ltl.simp (gen_1_form_universality (globalAlphabet alpha))
    else if (kind="bexist") then Ltl.simp (gen_1_form_boundedexistence (globalAlphabet alpha))
    else if (kind="prec") then Ltl.simp (gen_1_form_precedence (globalAlphabet alpha))
    else if (kind="resp") then Ltl.simp (gen_1_form_response (globalAlphabet alpha))
    else if (kind="pchain") then Ltl.simp (gen_1_form_precedence_chain (globalAlphabet alpha))
    else if (kind="rchain") then Ltl.simp (gen_1_form_response_chain (globalAlphabet alpha))
    else if (kind="cchain") then Ltl.simp (gen_1_form_constrained_chain (globalAlphabet alpha))
    else Ltl.simp (gen_1_form_abscence (globalAlphabet alpha)) in
  (* print_endline(string_rep_input(formula)); *)
  let (formula, trace,
       verdict_cent, verdict_Mdecent, verdict_Odecent,
       traceNeededCent,traceNeededMDeCent,traceNeededODecent,
       ncent, nMDecent, nODecent,
       smesscent, smessdecent, smessodecent,
  nbProgCent, nbProgDecent, nbProgODecent) = 
    compare_results formula alpha trace in
  let meaningful = verdict_cent = verdict_Mdecent && verdict_cent = verdict_Odecent && List.length traceNeededCent < size_trace && List.length traceNeededMDeCent < size_trace && List.length traceNeededODecent < size_trace && List.length traceNeededCent > 10 in
  (meaningful, List.length traceNeededCent + 1, List.length traceNeededMDeCent + 1, List.length traceNeededODecent + 1,ncent,nMDecent,nODecent,smesscent,smessdecent,smessodecent, nbProgCent, nbProgDecent, nbProgODecent)


(**
   /////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////
   Assessing the effect of 
   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////**)


let create_static_network (phi:dltl) (archi: architecture) : network =
  let chc_phi = chc phi archi 0 in
  let empty_network = make_empty_network (Architecture.cardinal archi) in
  let net = network archi chc_phi empty_network phi in
  let compactnet = compact net in
  reindex compactnet

let depth_with_one_dalpha (dalpha:d_alphabet) (form:dltl) : int =
  let archi = dalphabet_2_architecture dalpha in
  let net = create_static_network form archi in
  depth_network net archi form

let has_all_depths (alpha_size:int) (form:dltl) (dalphas:d_alphabet list) : bool =
  let has_given_depth (d:int) : bool =
    List.exists (fun elt -> depth_with_one_dalpha elt form = d) dalphas in
  let ok = ref true in
  for depth = 1 to alpha_size do
    ok := !ok && has_given_depth depth
  done;
  !ok
  
let compute_one_result (dalpha:d_alphabet) (trace:trace) (formula:ltl) =
  let size_trace = List.length trace in
  let dtrace = trace2dtrace trace dalpha in
  let formulad = ltl_2_dltl formula in
  let (formula, trace,
       verdict_cent, verdict_Mdecent, verdict_Odecent,
       traceNeededCent,traceNeededMDeCent,traceNeededODecent,
       ncent, nMDecent, nODecent,
       smesscent, smessdecent, smessodecent,
       nbProgCent, nbProgDecent, nbProgODecent) = 
    compare_results formula dalpha dtrace in
  let meaningful = verdict_cent = verdict_Mdecent && verdict_cent = verdict_Odecent && List.length traceNeededCent < size_trace && List.length traceNeededMDeCent < size_trace && List.length traceNeededODecent < size_trace in
  (meaningful, List.length dalpha, depth_with_one_dalpha dalpha formulad, List.length traceNeededCent, List.length traceNeededMDeCent, List.length traceNeededODecent, ncent,nMDecent, nODecent, smesscent, smessdecent, smessodecent, nbProgCent, nbProgDecent, nbProgODecent)

let rec make_one_eval_test (alpha:alphabet) (size_form:int) (size_trace:int) =
  let formula = Ltl.simp (Ltl_generator.gen_1_form_nontrivial size_form alpha) in
  let formulad = ltl_2_dltl formula in
  let dalphabets = generate_all_compatible_dalphabets alpha in
  let trace = gen_1_trace size_trace alpha in
  if has_all_depths (length alpha) formulad dalphabets then
    List.fold_left
      (fun acc elt -> (compute_one_result elt trace formula)::acc)
      [] dalphabets  
  else
    make_one_eval_test alpha size_form size_trace
